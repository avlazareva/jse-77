package ru.t1.lazareva.tm.constant;

import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.lazareva.tm.dto.model.SessionDto;
import ru.t1.lazareva.tm.dto.model.UserDto;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

@UtilityClass
public final class SessionTestData {

    @Nullable
    public final static String USER_1_LOGIN = "USER_1_LOGIN";

    @Nullable
    public final static String USER_1_PASSWORD = "USER_1_PASSWORD";

    @Nullable
    public final static String USER_1_EMAIL = "USER_1_EMAIL";

    @Nullable
    public final static String USER_2_LOGIN = "USER_2_LOGIN";

    @Nullable
    public final static String USER_2_PASSWORD = "USER_2_PASSWORD";

    @Nullable
    public final static String USER_2_EMAIL = "USER_2_EMAIL";

    @NotNull
    public final static UserDto USER_1 = new UserDto(USER_1_LOGIN, USER_1_PASSWORD, USER_1_EMAIL);
    @NotNull
    public final static SessionDto USER_1_SESSION = new SessionDto(USER_1);
    @NotNull
    public final static UserDto USER_2 = new UserDto(USER_2_LOGIN, USER_2_PASSWORD, USER_2_EMAIL);
    @NotNull
    public final static SessionDto USER_2_SESSION = new SessionDto(USER_2);
    @NotNull
    public final static List<SessionDto> USER_SESSION_LIST = Arrays.asList(USER_1_SESSION, USER_2_SESSION);
    @Nullable
    public final static SessionDto NULL_SESSION = null;
    @NotNull
    public final static String NON_EXISTING_SESSION_ID = UUID.randomUUID().toString();
    @Nullable
    public final static String SESSION_NAME = "SESSION_NAME";

    @Nullable
    public final static String SESSION_DESC = "SESSION_DESC";

}