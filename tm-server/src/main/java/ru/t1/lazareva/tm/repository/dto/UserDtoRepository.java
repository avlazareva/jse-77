package ru.t1.lazareva.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.lazareva.tm.dto.model.UserDto;
import ru.t1.lazareva.tm.enumerated.Role;

@Repository
@Scope("prototype")
public interface UserDtoRepository extends AbstractDtoRepository<UserDto> {

    @NotNull
    UserDto create(@NotNull final String login, @NotNull final String password);

    @NotNull
    UserDto create(
            @NotNull final String login,
            @NotNull final String password,
            @Nullable final String email
    );

    @NotNull
    UserDto create(
            @NotNull final String login,
            @NotNull final String password,
            @Nullable final Role role
    );

    @Nullable
    UserDto findByLogin(@NotNull final String login);

    @Nullable
    UserDto findByEmail(@NotNull final String email);

    Boolean isLoginExists(@NotNull final String login);

    Boolean isEmailExists(@NotNull final String email);

}


