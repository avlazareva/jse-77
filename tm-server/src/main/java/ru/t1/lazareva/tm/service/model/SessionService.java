package ru.t1.lazareva.tm.service.model;

import lombok.NoArgsConstructor;
import org.springframework.stereotype.Service;
import ru.t1.lazareva.tm.api.service.model.ISessionService;
import ru.t1.lazareva.tm.model.Session;
import ru.t1.lazareva.tm.repository.model.SessionRepository;

@Service
@NoArgsConstructor
public final class SessionService extends AbstractUserOwnedService<Session, SessionRepository> implements ISessionService {

}
