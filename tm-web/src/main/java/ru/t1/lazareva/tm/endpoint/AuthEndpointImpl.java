package ru.t1.lazareva.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import ru.t1.lazareva.tm.model.CredentialsDto;
import ru.t1.lazareva.tm.model.Result;
import ru.t1.lazareva.tm.model.UserDTO;
import ru.t1.lazareva.tm.repository.UserDtoRepository;

import javax.annotation.Resource;

@RestController
@RequestMapping("api/auth")
public class AuthEndpointImpl {

    @Resource
    private AuthenticationManager authenticationManager;

    @Autowired
    private UserDtoRepository userDTORepository;

    @NotNull
    @PostMapping("/login")
    public Result login(@NotNull @RequestBody CredentialsDto credentials) {
        try {
            final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(credentials.getUsername(), credentials.getPassword());
            final Authentication authentication = authenticationManager.authenticate(token);
            SecurityContextHolder.getContext().setAuthentication(authentication);
            return new Result(authentication.isAuthenticated());
        } catch (final Exception exception) {
            return new Result(exception);
        }
    }

    @Nullable
    @GetMapping
    public UserDTO profile() {
        final SecurityContext securityContext = SecurityContextHolder.getContext();
        final Authentication authentication = securityContext.getAuthentication();
        final String username = authentication.getName();
        return userDTORepository.findByLogin(username);
    }

    @NotNull
    @PostMapping("/logout")
    public Result logout() {
        SecurityContextHolder.getContext().setAuthentication(null);
        return new Result();
    }

}