package ru.t1.lazareva.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class Result {

    private Boolean success = true;

    private String message = "";

    public Result(String message) {
        this.message = message;
    }

    public Result(Boolean success) {
        this.success = success;
    }

    public Result(Exception e) {
        success = false;
        message = e.getMessage();
    }

}